
/*#Something*/

/*##Something variables*/

/*##Something functions*/

/*##Something init*/

/*##Something repeat*/

/*#Caret*/

/*##Caret variables*/
var carets=[], stop = false;

/*##Caret functions*/

Keys = function(){
}

Keys.prototype.newKey = function(key){
	this[key]=true;
}

keys = new Keys();

Caret = function(options){
	define(this,options,{
		context:matrice,
		x:0,
		y:0,
		x1:0,
		y1:0,
		color:'blue',
		text:'rgb(255,240,255)',
		draw:false,
		drawColor:'transparent',
		command:'',
		commands:[],
		/*toy*/
		moveX:0,
		moveY:0,
		play:false,
		char:'☺'
	});
	carets.push(this);
}

Caret.prototype.move = async function(t){

	if(this.play){
		if(this.x+this.moveX>0 && this.x+this.moveX<this.context.x-1){
			this.x+=this.moveX;
		}
		if(this.y+this.moveY>0 && this.y+this.moveY<this.context.y-1){
			this.y+=this.moveY;
		}
	}else{
		return
	}

	await sleep(t);
	if(this.draw){
		this.context.lines[this.y].types[this.x]['background-color']=this.drawColor;
	}
	//this.context.lines[this.y].types[this.x].update();
	this.move(t);
}

Caret.prototype.display = async function(colors,t){
	
	var type=this.context.lines[this.y].types[this.x];
	type.dom.style.backgroundColor=this.color;
	if(this.color!=colors[0]){
		this.color=colors[0];
		await sleep(t);
	}else{
		this.color=colors[1];
		await sleep(t/2);

	}
	//type.update();
	this.display(colors,t);
}

Caret.prototype.paste = async function(str,t){
	for(var i = 0 ; i< str.length; i++){
		await sleep(t);
		this.update(keys,str[i]);
	}
}

Caret.prototype.log = async function(str,t){
	this.x=0;
	if(this.y<this.context.y-1){
		this.y++;
	}
	str+='↵';
	for(var i = 0 ; i< str.length; i++){
		await sleep(t);
		if(this.x<this.context.x-1){
		}else{
			if(this.y<this.context.y-1){
				if(this.draw){
					this.context.lines[this.y].types[this.x]['background-color']=this.drawColor;
				}
				this.context.lines[this.y].types[this.x].update();
				this.y++;
				this.x=0;
			}
		}
		this.update(keys,str[i]);
	}

	/*this.command='';*/
}

Caret.prototype.update = function(key,char){
	this.context.lines[this.y].types[this.x].dom.style.backgroundColor=this.color;
	if(this.draw){
		this.context.lines[this.y].types[this.x]['background-color']=this.drawColor;
	}
	this.context.lines[this.y].types[this.x].update(100);
	
	if(char == '☺'){
		this.command='';
	}

	if (key['ArrowUp']){ 
		if(this.y>0){
			this.y--;
		}
	};

	if (key['ArrowDown']){
		if(this.y<this.context.y-1){
			this.y++;
		}
	};

	if (key['ArrowLeft']){ 
		if(this.x>0){
			this.x--;
		}
	};

	if (key['ArrowRight']){
		if(this.x<this.context.x-1){
			this.x++;
		}
	};

	if(char=='↵' || char=='Enter'){
		this.x=0;
		this.y++;
	}
	if(char == 'Home'){
		this.x=0;
	}
	if(char == 'End'){
		this.x=this.context.x-1;
	}
	if(char == 'PageUp'){
		this.x=0;
		this.y=0;
	}
	if(char == 'PageDown'){
		this.x=this.context.x-1;
		this.y=this.context.y-1;
	}

	if(char.length==1 && char!='↵'){
		if(this.draw){
			this.context.lines[this.y].types[this.x]['background-color']=this.drawColor;
		}
		this.context.lines[this.y].types[this.x].color=this.text;
		this.context.lines[this.y].types[this.x].char=char;
		this.context.lines[this.y].types[this.x].update();
		this.command+=char;
		if(this.x<this.context.x-1){
			this.x++;
			if(this.x==this.context.x-1){
				this.x=0;
				this.y++;
			}
		}
	}

	if(char == 'Backspace'){
		this.command=this.command.slice(0, -1);
		if(this.x>0){
			var line_chars=this.context.lines[this.y].dom.textContent;
			var x_index=0;
			for(var i = this.x ; i>0 ; i-- ){
				/*console.log(line_chars[i]);*/
				if(line_chars[i]!=' ' && line_chars[i]!=' '){
					x_index=i;
					break;
				}
			}
			/*console.log(x_index+1,this.x);*/
			if(x_index+1==this.x){
				this.x--;
			}else{
				if(x_index==0){
					this.x=0;
					this.update([],'Backspace');
				}else{
					if(x_index+1<this.context.x){
						this.x=x_index+1;
					}
				}
			}

			this.context.lines[this.y].types[this.x].char=' ';
		}else{
			if(this.y>0){
				var line_chars=this.context.lines[this.y-1].dom.textContent;
				var x_index=0;
				for(var i = line_chars.length-1 ; i>0 ; i-- ){
					console.log(line_chars[i]);
					if(line_chars[i]!=' ' && line_chars[i]!=' ' ){
						x_index=i;
						break;
					}

				}
				this.y--;
				if(x_index==0){
					this.update([],'Backspace');
				}else{
					this.x=x_index+1;
				}
			}
		}
	}

	if(char=='#'){
		this.command='';
		this.x1=this.x;
		this.y1=this.y;
	}

	if(char=='/'){
		this.command=this.commands[this.commands.length-1];
	}

	if(char=='%'){
		this.command=this.command.slice(0, -1);
		this.command+=' ';
		try {
			eval(this.command);
		}catch(e){
			/*this.log(this.command);*/
			this.log(e.message);
		}
		this.commands.push(this.command);
		this.command='';
	}

	//this.context.lines[this.y].types[this.x]['background-color']=this.color;
	this.context.lines[this.y].types[this.x].update();
}

Caret.prototype.clean = async function(options){
	for(var i in types){
		types[i]['background-color']='transparent';
		types[i].update();
	}

}

Caret.prototype.floodfill = async function(options){
	if(this.x-this.command.length-1<0 || stop){
		return
	}
	var local={};
	var defaultColor=this.context.lines[this.y1].types[this.x1]['background-color'];

	define(local,options,{
		x:this.x1,
		y:this.y1,
		context:this.context,
		color:cssColors[random(0,cssColors.length).int],
		color2:defaultColor,
		time:100,
		origin:true,
		top:true,
		right:true,
		left:true,
		bottom:true,
	});

	if(local.x>-1 && local.x<local.context.x && local.y>-1 && local.y<local.context.y){
	}else{
		return
	}

	var glyph=local.context.lines[local.y].types[local.x];

	var floodArg = [];
	var x = local.x;
	var y = local.y;
	var color = glyph['background-color'];

	if (color != local.color) {
		/*console.log(local.x);*/
		if(local.color2!='none'){
			if(color != local.color2){
				return
			}
		}
		/*console.log(color,local.color);*/
		glyph['background-color']=local.color;

		var rx = x+1;
		var lx = x-1;
		var by = y+1;
		var ty = y-1;

		if (local.top) {
			floodArg.push([x,ty]);
		}
		if (local.right) {
			floodArg.push([rx,y]);
		}
		
		if (local.bottom) {
			floodArg.push([x,by]);
		}
		if (local.left) {
			floodArg.push([lx,y]);
		}

		glyph.update();
		await sleep(local.time);
		for(var i in floodArg) {
			this.floodfill({
				x:floodArg[i][0],
				y:floodArg[i][1],
				context:local.context,
				color:local.color,
				color2:local.color2,
				time:local.time,
				origin:local,
				top:local.top,
				right:local.right,
				left:local.left,
				bottom:local.bottom,
			})
		}

		return

	}

}

/*##Caret init*/
init_functions.push(async function(){
	keydown_functions.push(function(e){
		textInput.focus();
		if(keys[e.key]==undefined){
			keys.newKey(e.key);
		}else{
			keys[e.key]=true;
		}

		/*if(keys['Control']==false && keys['v']==false){*/
			caret.update(keys,e.key);
			/*}*/
		});
	keyup_functions.push(function(e){
		console.log(e.key);
		/*
		if(keys['Control'] && keys['v']){
			console.log(textInput.value);
			caret.paste(textInput.value);
			textInput.value='';
		}
		*/
		if(keys[e.key]==undefined){
			keys.newKey(e.key);
		}else{
			keys[e.key]=false;
		}
	});
	var caret = new Caret({context:matrice});
	caret.display(['white','black'],750);
	await sleep(250);
	caret.y=-1;
	caret.log('↵Bienvenue dans Debussick ☺',50);
});
	/*##Caret repeat*/