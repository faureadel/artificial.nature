
/*#Something*/

/*##Something variables*/

/*##Something functions*/

/*##Something init*/

/*##Something repeat*/

/*#Setup*/

/*##Setup variables*/

init_functions=[],repeat_functions=[],keyup_functions=[],keydown_functions=[],textInput = document.createElement('textArea');;

/*##Setup functions*/

function init(){
	for(var i in init_functions){
		init_functions[i]();
	}
}

function repeat(){
	for(var i in repeat_functions){
		repeat_functions[i]();
	}
}

function keydown(e){
	for(var i in keydown_functions){
		keydown_functions[i](e);
	}
}

function keyup(e){
	for(var i in keyup_functions){
		keyup_functions[i](e);
	}
}

/*##Setup init*/

init_functions.push(function(){
	
	document.body.appendChild(textInput);

	style.all=newCssRule('*',{
		'font-family':'consolas,monospace',
		margin:0,
		padding:0,
		color:'rgb(255,240,255)',
	});
	style.html=newCssRule('html',{
		'background-color':'rgb(24,32,32);',
		overflow:'hidden',
		height:'200vw',
		width:'200vw'
	});
	style.tr=newCssRule('tr',{
		overflow:'hidden',
		display:'block',
		width:'200vw'
	})
	style.table=newCssRule('table',{
		'border-collapse': 'collapse',
		'outline':'solid',
		'outline-width':'1px',
		'outline-color':'rgb(255,240,255)'
	});
	style.textArea=newCssRule('textArea',{
		border:'none',
		position:'absolute',
		width:'0',
		height:'0',
	});
});


/*#keys*/

/*##keys variables*/

/*##keys functions*/

Keys = function(){
}

Keys.prototype.newKey = function(key){
	this[key]=true;
}

/*##keys init*/

init_functions.push(async function(){
	keys = new Keys();
	keydown_functions.push(function(e){
		if(keys[e.key]==undefined){
			keys.newKey(e.key);
		}else{
			keys[e.key]=true;
		}
	});
	keyup_functions.push(function(e){
		if(keys[e.key]==undefined){
			keys.newKey(e.key);
		}else{
			keys[e.key]=false;
		}
	});
	
});

/*##keys repeat*/