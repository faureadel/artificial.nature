
/*#Something*/

/*##Something variables*/

/*##Something functions*/

/*##Something init*/

/*##Something repeat*/

/*#Type*/

/*##Type variables*/

var types=[];

/*##Type functions*/

Type = function(options){

	define(this,options,{
		id:undefined,
		context:undefined,
		char:' ',
		'text-color':'rgb(255,240,255)',
		'background-color':'transparent',
		dom:document.createElement('td')
	});
	console.log(this.char);
	this.update();
	this.context.add(this);
	types.push(this);

}

Type.prototype.update = async function(t){
	if(t==undefined){
		t=0;
	}
	await sleep(t);
	updateCSS(this.dom,{
		color:this['text-color'],
		'background-color':this['background-color']
	})
	this.dom.textContent=this.char
}

/*##Type init*/

/*##Type repeat*/