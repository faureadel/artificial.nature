
/*#Something*/

/*##Something variables*/

/*##Something functions*/

/*##Something init*/

/*##Something repeat*/

/*#Matrice*/

/*##Matrice variables*/

matrices=[];

/*##Matrice functions*/

Matrice = function(options){

	define(this,options,{
		context:undefined,
		x:104,
		y:24,
		dom:document.createElement('table'),
		lines:[]
	})

	matrices.push(this);

}

Matrice.prototype.build = function(){

	for(var y = 0 ; y < this.y ; y++){
		var line = new Line({
			id:y,
			context:this
		});

		for(var x = 0 ; x < this.x ; x++){
			var type = new Type({
				id:x,
				context:line
			});
		}
	}

}

Matrice.prototype.add= function(line){

	line.context=this;
	this.lines.push(line);
	this.dom.appendChild(line.dom);

}

/*##Matrice init*/

matrice = new Matrice();
matrice.build();

init_functions.push(function(){
	document.body.appendChild(matrice.dom);
});