
/*#Something*/

/*##Something variables*/

/*##Something functions*/

/*##Something init*/

/*##Something repeat*/

/*#Line*/

/*##Line variables*/

var lines=[];

/*##Line functions*/

Line = function(options){

	define(this,options,{
		id:undefined,
		context:undefined,
		types:[],
		dom:document.createElement('tr')
	})

	this.context.add(this);
	lines.push(this);

}

Line.prototype.add = function(type){
	type.context=this;
	this.types.push(type);
	this.dom.appendChild(type.dom);
}

/*##Line init*/

	/*##Line repeat*/