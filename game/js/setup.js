/*#Setup*/

/*##Setup variables*/

init_functions=[],repeat_functions=[],keyup_functions=[],keydown_functions=[],textInput = document.createElement('textArea');

/*##Setup functions*/

function init(){
	console.log('init:start');
	for(var i in init_functions){
		init_functions[i]();
	}
	console.log('init:done');
}

function repeat(){
	for(var i in repeat_functions){
		repeat_functions[i]();
	}
}

function keydown(e){
	for(var i in keydown_functions){
		keydown_functions[i](e);
	}
}

function keyup(e){
	for(var i in keyup_functions){
		keyup_functions[i](e);
	}
}

/*##Setup init*/

/*#keys*/

/*##keys variables*/

/*##keys functions*/

Keys = function(){
}

Keys.prototype.newKey = function(key){
	this[key]=true;
}

/*##keys init*/

init_functions.push(async function(){
	keys = new Keys();
	keydown_functions.push(function(e){
		if(keys[e.key]==undefined){
			keys.newKey(e.key);
		}else{
			keys[e.key]=true;
		}
	});
	keyup_functions.push(function(e){
		if(keys[e.key]==undefined){
			keys.newKey(e.key);
		}else{
			keys[e.key]=false;
		}
	});
	
});

/*##keys repeat*/

/*#Something*/

/*##Something variables*/

/*##Something functions*/

/*###Something object*/

/*###Something proto*/

/*##Something init*/

init_functions.push(function(){

});

/*##Something repeat*/

repeat_functions.push(function(){

});