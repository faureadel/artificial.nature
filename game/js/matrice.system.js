/*#Journey*/

/*##Journey variables*/

/*##Journey functions*/

/*###Journey object*/

Journey = function(options){
	define(this,options,{
		/*system*/
		matrices:[],
		/*setting*/
		dom:document.body,
	});
}

/*###Journey proto*/

/*##Journey init*/

init_functions.push(function(){

	var size=parseInt(window.innerHeight/64);

	style.all=newCssRule('*',{
		'font-family':'consolas,monospace',
		margin:0,
		padding:0,
		color:'rgb(255,240,255)',
	});
	style.html=newCssRule('html',{
		'background-color':'rgb(24,32,32);',
		overflow:'hidden',
		height:'200vw',
		width:'200vw'
	});
	style.table=newCssRule('table',{
		'border-collapse': 'collapse',
		'outline':'solid',
		'outline-width':'1px',
		'outline-color':'rgb(255,240,255)'
	});
	style.tr=newCssRule('tr',{
		overflow:'hidden',
		display:'block',
		width:'200vw'
	})
	style.td=newCssRule('td',{
		display:'inline',
		float:'left',
		width:size+'px',
		height:size+'px',
		'font-size':size+'px',
		'text-align':'center',
		color:'rgb(255,240,255)',
		'background-repeat':'no-repeat',
		'background-size':'contain',
	});

	window.rt = new Journey();
	screen = new Matrice({name:'screen',context:rt});
	screen.build([parseInt(window.innerWidth/size)+3,parseInt(window.innerHeight/size)+3]);
	screen.context.dom.appendChild(screen.dom);
	/*matrice.testPattern();*/

});

/*##Journey repeat*/

/*#Matrice*/

/*##Matrice variables*/

/*##Matrice functions*/

/*###Matrice object*/

Matrice = function(options){
	define(this,options,{
		/*system*/
		context:undefined,
		lines:[],
		tabl:[],
		/*self*/
		name:Math.random(),
		x:undefined,
		y:undefined,
		/*setting*/
		dom:document.createElement('table'),
	});
	console.log(this);
	this.context.matrices[this.name]=this;
}


/*###Matrice proto*/

Matrice.prototype.build = function(size){

	this.x=size[0];
	this.y=size[1];

	for(var y = 0 ; y < this.y ; y++){

		var line = new Line({context:this,pos:y});
		var tablLine=[];
		for(var x = 0 ; x < this.x ; x++){
			var type = new Type({char:'',pos:x,context:line});
			tablLine.push(type);
		}
		this.tabl.push(tablLine);
	}

	console.log(this);
}

Matrice.prototype.testPattern = async function(pattern){
	console.log('testPattern:start');
	if(pattern==undefined){
		pattern=this.pattern0();
	}
	for(var l in this.lines){
		/*await sleep();*/
		for(var t in this.lines[l].types){
			/*await sleep();*/
			for(var i in pattern[l*t]){
				/*await sleep();*/
				this.lines[l].types[t][i]=pattern[l*t][i];
				this.lines[l].types[t].updateDom();
			}
		}
	}
	console.log('testPattern:done');
}

Matrice.prototype.pattern0 = function(){
	var pattern = [];
	for(var i = 0 ; i < this.x*this.y ; i++ ){
		var rdm = Math.random();
		if(rdm>0.5){
			pattern.push({
				color:[[255,255,255],[0,0,0]],
				char:'1',
			})
		}else{
			pattern.push({
				color:[[0,0,0],[255,255,255]],
				char:'0',
			})
		}
	}
	return pattern;
}

Matrice.prototype.pattern1 = function(){
	var pattern = [];
	for(var i = 0 ; i < this.x*this.y ; i++ ){
		pattern.push({
			color:[[255,196,128],[0,0,0]],
		})
	}
	return pattern;
}

Matrice.prototype.pattern2 = function(){
	var pattern = [];
	for(var i = 0 ; i < this.x*this.y ; i++ ){
		pattern.push({
			color:[[128,196,255],[0,0,0]],
		})
	}
	return pattern;
}

Matrice.prototype.pop = function(){
	var x = random(0,this.x).int;
	var y = random(0,this.y).int;
	if(this.tabl[y][x]){
		if(this.tabl[y][x].input){
			return
		}else{
			gizmo = new Gizmo({context:this,pos:[x,y]});
		}
	}else{
		return
	}
}

/*##Matrice init*/

init_functions.push(function(){


});

/*#Line*/

/*##Line variables*/

/*##Line functions*/

/*###Line object*/

Line = function(options){
	define(this,options,{
		/*system*/
		pos:undefined,
		context:undefined,
		types:[],
		/*self*/
		textContent:'',
		/*setting*/
		dom:document.createElement('tr'),
	});
	this.context.lines[this.pos]=this;
	this.context.dom.appendChild(this.dom);
}

/*###Line proto*/

Line.prototype.read = function() {
	var reading='';
	for(var i in this.types){
		reading+=this.types[i].char;
	}
	this.textContent=reading;
	return reading;
};

/*##Line init*/

/*##Line repeat*/

/*#Type*/

/*##Type variables*/

/*##Type functions*/

/*###Type object*/

Type = function(options){
	define(this,options,{
		/*system*/
		pos:undefined,
		context:undefined,
		output:null,
		input:null,
		/*self*/
		char:' ',
		image:'none',
		color:[/*background*/[127,127,127],/*char*/[0,0,0]],
		/*setting*/
		dom:document.createElement('td'),
	});
	
	try{
		this.context.types[this.pos]=this;
		this.context.dom.appendChild(this.dom);
	}catch(e){

	}
	this.updateDom();
}

/*###Type proto*/

Type.prototype.updateDom=function(){
	if(this.input!=null){
		var update={
			textContent:this.input.char,
			style:{
				color:'rgb('+this.input.color[1][0]+','+this.input.color[1][1]+','+this.input.color[1][2]+')',
				backgroundColor:'rgb('+this.input.color[0][0]+','+this.input.color[0][1]+','+this.input.color[0][2]+')',
			}
		}
	}else{
		var update={
			textContent:this.char,
			style:{
				color:'rgb('+this.color[1][0]+','+this.color[1][1]+','+this.color[1][2]+')',
				backgroundColor:'rgb('+this.color[0][0]+','+this.color[0][1]+','+this.color[0][2]+')',
			}
		}
	}
	if(this.output!=null){
		for(var i in update){
			if(typeof update[i] == 'object' ){
				for(var o in update[i]){
					this.output.dom[i][o]=update[i][o];
				}
			}else{
				this.output.dom[i]=update[i];
			}
		}
	}else{
		for(var i in update){
			if(typeof update[i] == 'object' ){
				for(var o in update[i]){
					this.dom[i][o]=update[i][o];
				}
			}else{
				this.dom[i]=update[i];
			}
		}
	}
}

/*##Type init*/

init_functions.push(function(){
});

/*##Type repeat*/

/*##keys repeat*/

/*#Camera*/

/*##Camera variables*/

/*##Camera functions*/

/*###Camera object*/

Camera = function(options){

	define(this,options,{
		x:0,
		y:0,
		input:undefined,
		output:undefined,
		types:[],
	})
	this.x=parseInt(this.input.x/2)+parseInt(-this.output.x/2+2);
	this.y=parseInt(this.input.y/2)+parseInt(-this.output.y/2+1);

	for(var i in this.output.lines){

		for(var j in this.output.lines[i].types){
			this.types.push(this.output.lines[i].types[j]);
		}
	}

	this.display(true);

}

/*###Camera proto*/

Camera.prototype.update = function(key){
	if (key['ArrowUp']){ 
		this.y--;
	};

	if (key['ArrowDown']){
		this.y++;
	};

	if (key['ArrowLeft']){ 
		this.x--;
	};

	if (key['ArrowRight']){
		this.x++;
	};
	this.display(true);
}

Camera.prototype.display = function(done){
	for(var i in this.types){
		try{
			var output=this.types[i];
			var input=this.input.lines[parseInt(this.types[i].context.pos)+this.y].types[parseInt(this.types[i].pos)+this.x];
			if(input!=undefined){
				output.input=input;
				input.output=output;
				input.updateDom();
				continue;
			}
			
		}catch(e){

		}
		if(output.input!=undefined){
			output.input.output=null;
			output.input=null;
			output.updateDom();
		}
	}
}

/*##Camera init*/

init_functions.push( async function(){

	map = new Matrice({name:'map',context:rt});
	map.build([32,32]);
	map.testPattern(map.pattern1());
	screen.testPattern(screen.pattern2());
	camera = new Camera({input:map,output:rt.matrices.screen});

	keydown_functions.push(function(e){
		camera.update(keys);




/*
	
*/

});	

	keyup_functions.push(function(e){

		//camera.update(keys);

	});	

});



/*##Camera repeat*/

