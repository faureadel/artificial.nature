/*#Journey*/

/*##Journey variables*/



/*##Journey functions*/

/*###Journey object*/

Journey = function(options){
	define(this,options,{
		/*system*/
		matrices:[],
		/*setting*/
		dom:document.body,
	});
}

/*###Journey proto*/

/*##Journey init*/

init_functions.push(function(){

	var size=parseInt(window.innerHeight/32);

	style.all=newCssRule('*',{
		'font-family':'consolas,monospace',
		margin:0,
		padding:0,
		color:'rgb(255,240,255)',
	});
	style.html=newCssRule('html',{
		'background-color':'rgb(24,32,32);',
		overflow:'hidden',
		height:'200vw',
		width:'200vw'
	});
	style.table=newCssRule('table',{
		'border-collapse': 'collapse',
		'outline':'solid',
		'outline-width':'1px',
		'outline-color':'rgb(255,240,255)'
	});
	style.tr=newCssRule('tr',{
		overflow:'hidden',
		display:'block',
		width:'200vw'
	})
	style.td=newCssRule('td',{
		display:'inline',
		float:'left',
		width:size+'px',
		height:size+'px',
		'font-size':size+'px',
		'text-align':'center',
		color:'rgb(255,240,255)',
		'background-repeat':'no-repeat',
		'background-size':'contain',
	});

	rt = new Journey();
	matrice = new Matrice({context:rt});
	matrice.build([parseInt(window.innerWidth/size)+3,parseInt(window.innerHeight/size)+3]);
	matrice.testPattern();

});

/*##Journey repeat*/

/*#Matrice*/

/*##Matrice variables*/

/*##Matrice functions*/

/*###Matrice object*/

Matrice = function(options){
	define(this,options,{
		/*system*/
		context:undefined,
		lines:[],
		/*self*/
		x:undefined,
		y:undefined,
		/*setting*/
		dom:document.createElement('table'),
	});
	this.context.dom.appendChild(this.dom);
}


/*###Matrice proto*/

Matrice.prototype.build = function(size){

	this.x=size[0];
	this.y=size[1];

	for(var y = 0 ; y < this.y ; y++){

		var line = new Line({context:this,pos:y});

		for(var x = 0 ; x < this.x ; x++){
			var type = new Type({char:'',pos:x,context:line});
			line.types.push(type);
		}

		this.lines.push(line);

	}

}

Matrice.prototype.testPattern = async function(pattern){
	console.log('testPattern:start');
	if(pattern==undefined){
		pattern = [];
		for(var i = 0 ; i < this.x*this.y ; i++ ){
			var rdm = Math.random();
			if(rdm>0.5){
				pattern.push({
					color:[[255,255,255],[0,0,0]],
					char:'1',
				})
			}else{
				pattern.push({
					color:[[0,0,0],[255,255,255]],
					char:'0',
				})
			}
		}
	}
	for(var l in this.lines){
		await sleep();
		for(var t in this.lines[l].types){
			await sleep();
			for(var i in pattern[l*t]){
				await sleep();
				this.lines[l].types[t][i]=pattern[l*t][i];
				this.lines[l].types[t].updateDom();
			}
		}
	}
	console.log('testPattern:done');
}

/*##Matrice init*/

init_functions.push(function(){


});

/*#Line*/

/*##Line variables*/

/*##Line functions*/

/*###Line object*/

Line = function(options){
	define(this,options,{
		/*system*/
		pos:undefined,
		context:undefined,
		types:[],
		/*self*/
		textContent:'',
		/*setting*/
		dom:document.createElement('tr'),
	});
	this.context.dom.appendChild(this.dom);
}

/*###Line proto*/

Line.prototype.read = function() {
	var reading='';
	for(var i in this.types){
		reading+=this.types[i].char;
	}
	this.textContent=reading;
	return reading;
};

/*##Line init*/

/*##Line repeat*/

/*#Type*/

/*##Type variables*/

/*##Type functions*/

/*###Type object*/

Type = function(options){
	define(this,options,{
		/*system*/
		pos:undefined,
		context:undefined,
		output:null,
		input:null,
		/*self*/
		char:' ',
		image:'none',
		color:[/*background*/[127,127,127],/*char*/[0,0,0]],
		/*setting*/
		dom:document.createElement('td'),
	});
	this.context.dom.appendChild(this.dom);
	this.updateDom();
}

/*###Type proto*/

Type.prototype.updateDom=function(){
	var update={
		textContent:this.char,
		style:{
			color:'rgb('+this.color[1][0]+','+this.color[1][1]+','+this.color[1][2]+')',
			backgroundColor:'rgb('+this.color[0][0]+','+this.color[0][1]+','+this.color[0][2]+')',
		}
	}
	for(var i in update){
		if(typeof update[i] == 'object' ){
			for(var o in update[i]){
				this.dom[i][o]=update[i][o];
			}
		}else{
			this.dom[i]=update[i];
		}
	}
}

/*##Type init*/

init_functions.push(function(){
});

/*##Type repeat*/
