/*#Gizmo*/

/*##Gizmo variables*/

/*##Gizmo functions*/

/*###Gizmo object*/

Gizmo = function(options){
	define(this,options,{
		/*system*/
		context:undefined,
		pos:undefined,
		/*self*/
		type:new Type({context:this,color:[/*background*/[random(0,255).int,random(0,255).int,random(0,255).int],/*char*/[0,0,0]]}),
		keys:new Keys(),
		crossable:false,
		speed:random(10,100).int,
		choices:['ArrowDown','ArrowUp','ArrowLeft','ArrowRight'],
		trials:{}
	});
	for(var i in this.choices){
		this.keys.trigger(this.choices[i],false);
	}
	this.update(true);
	this.learn('move');
}

/*###Gizmo proto*/

Gizmo.prototype.learn = async function(what){
	if(what=='move'){
		if(this.trials[what]){
			await sleep(1000);
			this.trials[what].push(this.try(function(g){g.move()},what));
			this.learn(what);
		}else{
			this.trials[what]=[];
			this.learn(what);
		}
		
		

	}
}

Gizmo.prototype.update = function(comin) {
	if(comin){
		this.context.lines[this.pos[1]].types[this.pos[0]].input=this.type;
		this.context.lines[this.pos[1]].types[this.pos[0]].updateDom();
	}else{
		this.context.lines[this.pos[1]].types[this.pos[0]].input=null;
		this.context.lines[this.pos[1]].types[this.pos[0]].updateDom();
	}

};

Gizmo.prototype.move = function(){

	var pos=[];

	pos[0]=this.pos[0];pos[1]=this.pos[1];

	if (this.keys['ArrowUp']){ 
		pos[1]--;
	};

	if (this.keys['ArrowDown']){
		pos[1]++;
	};

	if (this.keys['ArrowLeft']){ 
		pos[0]--;
	};

	if (this.keys['ArrowRight']){
		pos[0]++;
	};

	if(this.context.lines[pos[1]]){
		if(this.context.lines[pos[1]].types[pos[0]]){
			if(this.context.lines[pos[1]].types[pos[0]].input){
				return true
			}
		}else{
			return true
		}		
	}else{
		return true
	}
	if(pos[0]==this.pos[0] && pos[1]==this.pos[1]){
		return true
	}

	this.update(false);
	this.pos=pos;
	this.update(true);

}

Gizmo.prototype.try = function(what,name){

	var input=[];

	for(var i in this.keys){
		input.push(this.keys[i]);
	}

	var trial = {
		input:input,
		output:undefined,
		pass:undefined
	}

	var choices=[];

	for(var t in this.trials[name]){
		if(arraysEqual(input,this.trials[name][t].input) && this.trials[name][t].pass){
			choices.push(this.trials[name][t]);
		}
	}

	if(choices.length==0){
		for(var i in this.choices){
			var rdm=Math.random();
			if(rdm>0.5){
				this.keys.trigger(this.choices[i],false);
			}else{
				this.keys.trigger(this.choices[i],true);
			}
		}
	}else{
		for(var i in this.choices){
		this.keys.trigger(this.choices[i],false);
	}
		console.log(choices);
	}

	var output=[];
	
	for(var i in this.keys){
		output.push(this.keys[i]);
	}

	trial.output=output;

	if(what(this)){
		trial.pass=false;
	}else{
		trial.pass=true;
	}

	return trial

}
/*
Gizmo.prototype.walk = async function(){

	await sleep(this.speed);

	
	if(this.move(this.keys)){
		//this.think(this.keys);
	}		
	
	this.walk();

}
*/

/*##Gizmo init*/

init_functions.push(function(){

});

/*##Gizmo repeat*/

repeat_functions.push(function(){

});